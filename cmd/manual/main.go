package main

import (
	"fmt"
	"gitlab.com/dualwield/golib/chrypto"
)

func main() {
	key := chrypto.ScaleKey([]byte("abcdefgkljdslkdjsdlksjdsldksjdlksdjslkdjsdskldjskldjslkdjsdklsjdslkjd"))
	data := []byte("slay not he that cannot hear, be thankful ye that hath an ear")
	fmt.Println(string(key))
	fmt.Println(string(data))

	enc, err := chrypto.Encrypt(key, data)
	if err != nil {
		fmt.Println(err)
		return
	}
	dec, err := chrypto.Dechrypt(key, enc)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(dec))

	code := make([]byte, 7)
	chrypto.RandomHumanCode(code)

	fmt.Println(string(code))
	cc := make([]byte, 32)
	chrypto.RandomComplexCode(cc)

	fmt.Println(string(cc))
}
