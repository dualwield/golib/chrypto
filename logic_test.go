package chrypto

import "testing"

// TestFullPath should cover most of the code. It's cheap, but I don't care
// right now.
func TestFullPath(t *testing.T) {
	key := RandomKey()
	data := []byte("Slay Not He That Cannot Hear.. Be thankful Ye That Hath An Ear")

	enc, err := Encrypt(key, data)
	if err != nil {
		t.Fatal(err)
	}
	dec, err := Dechrypt(key, enc)
	if err != nil {
		t.Fatal(err)
	}
	if string(dec) != string(data) {
		t.Fatalf("\ngot: %s\nexpecting: %s\n", string(dec), string(data))
	}
}
