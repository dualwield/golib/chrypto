package chrypto

import (
	"math/rand"
	"time"
)

const humanSymbols = `ABCDEDFGHIJKLMNPQRSTUVWXYZ0123456789`
const complexSymbols = `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*-+`

func randomCode(container []byte, source string, seed int64) {
	rand.Seed(seed)
	size := len(source)
	for i := range container {
		container[i] = source[rand.Intn(size)]
	}
}

func RandomHumanCode(container []byte) {
	seed := time.Now().UnixNano()
	randomCode(container, humanSymbols, seed)
}

func RandomComplexCode(container []byte) {
	seed := time.Now().UnixNano()
	randomCode(container, complexSymbols, seed)
}
