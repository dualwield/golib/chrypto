package chrypto

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
)

// ScaleKey creates an md5 hash of the passed in key. Giving a 32 byte key
func ScaleKey(key []byte) []byte {
	h := md5.New()
	_, err := h.Write(key)
	if err != nil {
		return nil
	}
	return []byte(hex.EncodeToString(h.Sum(nil)))
}

// RandomKey creates a random 32 byte key
func RandomKey() []byte {
	return RandomKeyBySize(32)
}

// RandomKeyBySize creates a random key with a length of passed in size
func RandomKeyBySize(size int) []byte {
	b := make([]byte, size)
	if _, err := rand.Read(b); err != nil {
		return nil
	}
	return b
}
