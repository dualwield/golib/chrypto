package chrypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"io"
)

func Encrypt(key, data []byte) (encrypted []byte, err error) {
	cypherBlock, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	cyphered := make([]byte, aes.BlockSize+len(data))
	iv := cyphered[:aes.BlockSize]

	_, err = io.ReadFull(rand.Reader, iv)
	if err != nil {
		return
	}

	cipher.NewCFBEncrypter(cypherBlock, iv).
		XORKeyStream(cyphered[aes.BlockSize:], data)

	encrypted = []byte(base64.URLEncoding.EncodeToString(cyphered))
	return
}
