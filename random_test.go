package chrypto

import "testing"

func Test_randomHumanCode(t *testing.T) {
	type args struct {
		container []byte
		seed      int64
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "human code",
			args: args{
				container: make([]byte, 7),
				seed:      2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			randomCode(tt.args.container, humanSymbols, tt.args.seed)
			if string(tt.args.container) != "8FL6U0J" {
				t.Errorf("\nexpected: %s\nactual: %s", "8FL6U0J", tt.args.container)
			}
		})
	}
}
