package chrypto

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"
)

func Dechrypt(key []byte, enchryptedData []byte) (dechrypted []byte, err error) {
	cypherData, err := base64.URLEncoding.DecodeString(string(enchryptedData))
	if err != nil {
		return
	}

	cypherBlock, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	if len(cypherData) < aes.BlockSize {
		err = errors.New("cypherblock too small")
		return
	}

	iv := cypherData[:aes.BlockSize]
	cypherData = cypherData[aes.BlockSize:]
	cipher.NewCFBDecrypter(cypherBlock, iv).XORKeyStream(cypherData, cypherData)

	dechrypted = cypherData
	return
}
