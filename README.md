# Chrypto
When a project gets to encryption.. I have found a sudden
blackhole of time spent getting it to work. This
library is to avoid rewriting this for the
upteenth time.

I gleaned the code for this from several sources
that I unfortunately don't remember.. but I can
say they were all very similar. Here is one
[here](https://itnext.io/encrypt-data-with-a-password-in-go-b5366384e291).. which doesn't
match what this lib is doing, but it's similar.

## Key Scaling
A helper function `ScaleKey(key []byte)` converts
the key into an md5 value.., length 32. Since
the key should be 32 bytes.